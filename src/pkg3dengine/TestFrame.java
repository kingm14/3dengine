/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dengine;

import java.awt.*;
import java.util.*;
import java.awt.Graphics;
import java.util.*;
import java.applet.Applet;
import java.awt.*;
import java.applet.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.*;

/**
 *
 * @author kingm14
 */
public class TestFrame extends javax.swing.JFrame implements Runnable, KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

    /**
     * Creates new form TestFrame
     */
    private ArrayList<Point3D> nodes;
    private ArrayList<Point> displayNodes;
    private Matrix nodeMatrix;
    private Matrix modifiedMatrix;
    private Point3D myPos;
    private Thread animation = null;
    Graphics offScreen;  //NEW!
    Image image;         //NEW!
    private boolean upPressed;
    private boolean downPressed;
    private boolean leftPressed;
    private boolean rightPressed;
    private boolean plusPressed;
    private boolean minusPressed;
    private double shiftX;
    private double shiftY;
    private double shiftZ;
    private double rotX;
    private double rotY;
    private double rotZ;
    private double rotXSum;
    private double rotYSum;
    private double rotZSum;
    private boolean isAltDown;
    private Scanner scanner;
    private MyEntity entity;
    private ArrayList<Vertex> simulSelectedVertices;

    public TestFrame() {
        addKeyListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        simulSelectedVertices = new ArrayList<Vertex>();
        selectedV = null;
        initComponents();
        scanner = new Scanner(System.in);
        myPos = new Point3D(10, 10, 10);
        nodes = new ArrayList<Point3D>();
        displayNodes = new ArrayList<Point>();
        double[][] temp = new double[][]{
            {1, 1, 1, 1},
            {1, 1, -1, 1},
            {1, -1, 1, 1},
            {1, -1, -1, 1},
            {-1, 1, 1, 1},
            {-1, 1, -1, 1},
            {-1, -1, 1, 1},
            {-1, -1, -1, 1}
        };
        upPressed = false;
        downPressed = false;
        leftPressed = false;
        rightPressed = false;
        plusPressed = false;
        minusPressed = false;
        isAltDown = false;
        shiftX = 0;
        shiftY = 0;
        shiftZ = 0;
        rotX = 0;
        rotY = 0;
        rotZ = 0;
        nodeMatrix = new Matrix(temp).transpose();
        nodeMatrix.show();
        //System.out.println();
        modifiedMatrix = nodeMatrix;
        nodes.add(new Point3D(1, 1, 1));
        nodes.add(new Point3D(1, 1, -1));
        nodes.add(new Point3D(1, -1, 1));
        nodes.add(new Point3D(1, -1, -1));
        nodes.add(new Point3D(-1, 1, 1));
        nodes.add(new Point3D(-1, 1, -1));
        nodes.add(new Point3D(-1, -1, 1));
        nodes.add(new Point3D(-1, -1, -1));
        //entity=createPyramidAt(new Point3D(0,0,3),1,1,1);
        entity = createPlaneAt(new Point3D(0, 0, 0), 1, 1);
        entity.setToDefault();
        vertexList.setListData(entity.getAllVertices().toArray());
        init();
        start();


    }

    public MyEntity createPlaneAt(Point3D origin, double x, double z) {
        Point3D center = new Point3D(0, 0, 0);
        Vertex a = new Vertex(center.getX() - (x / 2), 0, center.getZ() - (z / 2), "a");
        Vertex b = new Vertex(center.getX() - (x / 2), 0, center.getZ() + (z / 2), "b");
        Vertex c = new Vertex(center.getX() + (x / 2), 0, center.getZ() + (z / 2), "c");
        Vertex d = new Vertex(center.getX() + (x / 2), 0, center.getZ() - (z / 2), "d");

        /*
         * Point3D center = new Point3D(0, 0, 0);
         Vertex a = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "a");
         Vertex b = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "b");
         Vertex c = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "c");
         Vertex d = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "d");
         Vertex e = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "e");
         Vertex f = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "f");
         Vertex g = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "g");
         Vertex h = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "h");

         ArrayList<Vertex> vList1 = TestFrame.toArrayList(new Vertex[]{f, b, a});//top
         ArrayList<Vertex> vList10 = TestFrame.toArrayList(new Vertex[]{a,e,f});
         ArrayList<Vertex> vList2 = TestFrame.toArrayList(new Vertex[]{e,a,c});//front
         ArrayList<Vertex> vList20 = TestFrame.toArrayList(new Vertex[]{c,g,e});
         ArrayList<Vertex> vList3 = TestFrame.toArrayList(new Vertex[]{g, c, d});//bottom
         ArrayList<Vertex> vList30 = TestFrame.toArrayList(new Vertex[]{d,h,g});
         ArrayList<Vertex> vList4 = TestFrame.toArrayList(new Vertex[]{b, f,h});//back
         ArrayList<Vertex> vList40 = TestFrame.toArrayList(new Vertex[]{ h,d,b});
         ArrayList<Vertex> vList5 = TestFrame.toArrayList(new Vertex[]{a, b, d});//left
         ArrayList<Vertex> vList50 = TestFrame.toArrayList(new Vertex[]{d, c,a});
         ArrayList<Vertex> vList6 = TestFrame.toArrayList(new Vertex[]{f, e, g});//right
         ArrayList<Vertex> vList60 = TestFrame.toArrayList(new Vertex[]{g, h,f});//right
         MyPolygon square1 = new MyPolygon(Color.green, vList1);
         MyPolygon square2 = new MyPolygon(Color.yellow, vList2);
         MyPolygon square3 = new MyPolygon(Color.orange, vList3);
         MyPolygon square4 = new MyPolygon(Color.red, vList4);
         MyPolygon square5 = new MyPolygon(Color.magenta, vList5);
         MyPolygon square6 = new MyPolygon(Color.BLUE, vList6);
         MyPolygon square10 = new MyPolygon(Color.green, vList10);
         MyPolygon square20 = new MyPolygon(Color.yellow, vList20);
         MyPolygon square30 = new MyPolygon(Color.orange, vList30);
         MyPolygon square40 = new MyPolygon(Color.red, vList40);
         MyPolygon square50 = new MyPolygon(Color.magenta, vList50);
         MyPolygon square60 = new MyPolygon(Color.BLUE, vList60);
        

         ArrayList<MyPolygon> faces = new ArrayList<MyPolygon>();
         faces.add(square1);
         faces.add(square2);
         faces.add(square3);
         faces.add(square4);
         faces.add(square5);
         faces.add(square6);
         faces.add(square10);
         faces.add(square20);
         faces.add(square30);
         faces.add(square40);
         faces.add(square50);
         faces.add(square60);
         MyEntity cube = new MyEntity(origin.getX(), origin.getY(), origin.getZ(), faces);
         return cube;
         */
        ArrayList<Vertex> vList1 = TestFrame.toArrayList(new Vertex[]{d, c, b, a});//top

        MyPolygon square1 = new MyPolygon(Color.white, vList1);


        ArrayList<MyPolygon> faces = new ArrayList<MyPolygon>();
        faces.add(square1);

        MyEntity cube = new MyEntity(origin.getX(), origin.getY(), origin.getZ(), faces);
        return cube;
    }

    public MyEntity createCubeAt(Point3D origin, double x, double y, double z) {
        Point3D center = new Point3D(0, 0, 0);
        Vertex a = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "a");
        Vertex b = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "b");
        Vertex c = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "c");
        Vertex d = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "d");
        Vertex e = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "e");
        Vertex f = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "f");
        Vertex g = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "g");
        Vertex h = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "h");
        /*
         * Point3D center = new Point3D(0, 0, 0);
         Vertex a = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "a");
         Vertex b = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "b");
         Vertex c = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "c");
         Vertex d = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "d");
         Vertex e = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "e");
         Vertex f = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "f");
         Vertex g = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "g");
         Vertex h = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "h");

         ArrayList<Vertex> vList1 = TestFrame.toArrayList(new Vertex[]{f, b, a});//top
         ArrayList<Vertex> vList10 = TestFrame.toArrayList(new Vertex[]{a,e,f});
         ArrayList<Vertex> vList2 = TestFrame.toArrayList(new Vertex[]{e,a,c});//front
         ArrayList<Vertex> vList20 = TestFrame.toArrayList(new Vertex[]{c,g,e});
         ArrayList<Vertex> vList3 = TestFrame.toArrayList(new Vertex[]{g, c, d});//bottom
         ArrayList<Vertex> vList30 = TestFrame.toArrayList(new Vertex[]{d,h,g});
         ArrayList<Vertex> vList4 = TestFrame.toArrayList(new Vertex[]{b, f,h});//back
         ArrayList<Vertex> vList40 = TestFrame.toArrayList(new Vertex[]{ h,d,b});
         ArrayList<Vertex> vList5 = TestFrame.toArrayList(new Vertex[]{a, b, d});//left
         ArrayList<Vertex> vList50 = TestFrame.toArrayList(new Vertex[]{d, c,a});
         ArrayList<Vertex> vList6 = TestFrame.toArrayList(new Vertex[]{f, e, g});//right
         ArrayList<Vertex> vList60 = TestFrame.toArrayList(new Vertex[]{g, h,f});//right
         MyPolygon square1 = new MyPolygon(Color.green, vList1);
         MyPolygon square2 = new MyPolygon(Color.yellow, vList2);
         MyPolygon square3 = new MyPolygon(Color.orange, vList3);
         MyPolygon square4 = new MyPolygon(Color.red, vList4);
         MyPolygon square5 = new MyPolygon(Color.magenta, vList5);
         MyPolygon square6 = new MyPolygon(Color.BLUE, vList6);
         MyPolygon square10 = new MyPolygon(Color.green, vList10);
         MyPolygon square20 = new MyPolygon(Color.yellow, vList20);
         MyPolygon square30 = new MyPolygon(Color.orange, vList30);
         MyPolygon square40 = new MyPolygon(Color.red, vList40);
         MyPolygon square50 = new MyPolygon(Color.magenta, vList50);
         MyPolygon square60 = new MyPolygon(Color.BLUE, vList60);
        

         ArrayList<MyPolygon> faces = new ArrayList<MyPolygon>();
         faces.add(square1);
         faces.add(square2);
         faces.add(square3);
         faces.add(square4);
         faces.add(square5);
         faces.add(square6);
         faces.add(square10);
         faces.add(square20);
         faces.add(square30);
         faces.add(square40);
         faces.add(square50);
         faces.add(square60);
         MyEntity cube = new MyEntity(origin.getX(), origin.getY(), origin.getZ(), faces);
         return cube;
         */
        ArrayList<Vertex> vList1 = TestFrame.toArrayList(new Vertex[]{f, b, a, e});//top
        ArrayList<Vertex> vList2 = TestFrame.toArrayList(new Vertex[]{e, a, c, g});//front
        ArrayList<Vertex> vList3 = TestFrame.toArrayList(new Vertex[]{g, c, d, h});//bottom
        ArrayList<Vertex> vList4 = TestFrame.toArrayList(new Vertex[]{h, d, b, f});//back
        ArrayList<Vertex> vList5 = TestFrame.toArrayList(new Vertex[]{a, b, d, c});//left
        ArrayList<Vertex> vList6 = TestFrame.toArrayList(new Vertex[]{f, e, g, h});//right
        MyPolygon square1 = new MyPolygon(Color.green, vList1);
        MyPolygon square2 = new MyPolygon(Color.yellow, vList2);
        MyPolygon square3 = new MyPolygon(Color.orange, vList3);
        MyPolygon square4 = new MyPolygon(Color.red, vList4);
        MyPolygon square5 = new MyPolygon(Color.magenta, vList5);
        MyPolygon square6 = new MyPolygon(Color.BLUE, vList6);

        ArrayList<MyPolygon> faces = new ArrayList<MyPolygon>();
        faces.add(square1);
        faces.add(square2);
        faces.add(square3);
        faces.add(square4);
        faces.add(square5);
        faces.add(square6);
        MyEntity cube = new MyEntity(origin.getX(), origin.getY(), origin.getZ(), faces);
        return cube;
    }

    public MyEntity createPyramidAt(Point3D origin, double x, double y, double z) {
        Point3D center = new Point3D(0, 0, 0);
        Vertex a = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "a");
        Vertex b = new Vertex(center.getX() - (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "b");
        Vertex c = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "c");
        Vertex d = new Vertex(center.getX() - (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "d");
        Vertex e = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() - (z / 2), "e");
        Vertex f = new Vertex(center.getX() + (x / 2), center.getY() - (y / 2), center.getZ() + (z / 2), "f");
        Vertex g = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "g");
        Vertex h = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "h");
        Vertex i = new Vertex(center.getX(), center.getY(), center.getZ(), "i");
        //ArrayList<Vertex> vList1=TestFrame.toArrayList(new Vertex[]{f,b,a,e});//top
        ArrayList<Vertex> vList2 = TestFrame.toArrayList(new Vertex[]{i, c, g});//front
        ArrayList<Vertex> vList3 = TestFrame.toArrayList(new Vertex[]{g, c, d, h});//bottom
        ArrayList<Vertex> vList4 = TestFrame.toArrayList(new Vertex[]{h, d, i});//back
        ArrayList<Vertex> vList5 = TestFrame.toArrayList(new Vertex[]{i, d, c});//left
        ArrayList<Vertex> vList6 = TestFrame.toArrayList(new Vertex[]{i, g, h});//right
        //MyPolygon square1=new MyPolygon(Color.green,vList1);
        MyPolygon square2 = new MyPolygon(Color.yellow, vList2);
        MyPolygon square3 = new MyPolygon(Color.orange, vList3);
        MyPolygon square4 = new MyPolygon(Color.red, vList4);
        MyPolygon square5 = new MyPolygon(Color.magenta, vList5);
        MyPolygon square6 = new MyPolygon(Color.BLUE, vList6);

        ArrayList<MyPolygon> faces = new ArrayList<MyPolygon>();
        //faces.add(square1);
        faces.add(square2);
        faces.add(square3);
        faces.add(square4);
        faces.add(square5);
        faces.add(square6);
        MyEntity cube = new MyEntity(origin.getX(), origin.getY(), origin.getZ(), faces);
        return cube;
    }

    public MyEntity createDiamondAt(Point3D origin, double x, double y, double z) {
        Point3D center = new Point3D(0, 0, 0);
        Vertex a = new Vertex(center.getX() - (x / 2), 0, center.getZ() - (z / 2), "a");
        Vertex b = new Vertex(center.getX() - (x / 2), 0, center.getZ() + (z / 2), "b");
        Vertex c = new Vertex(center.getX() + (x / 2), 0, center.getZ() + (z / 2), "c");
        Vertex d = new Vertex(center.getX() + (x / 2), 0, center.getZ() - (z / 2), "d");
        Vertex bottom = new Vertex(0, center.getY() - (y), 0, "bottom");
        Vertex top = new Vertex(0, center.getY() + (y), 0, "top");
        ArrayList<Vertex> vList1 = TestFrame.toArrayList(new Vertex[]{a, b, top});//front
        ArrayList<Vertex> vList2 = TestFrame.toArrayList(new Vertex[]{b, c, top});
        ArrayList<Vertex> vList3 = TestFrame.toArrayList(new Vertex[]{c, d, top});
        ArrayList<Vertex> vList4 = TestFrame.toArrayList(new Vertex[]{d, a, top});
        ArrayList<Vertex> vList5 = TestFrame.toArrayList(new Vertex[]{a, bottom, b});//front
        ArrayList<Vertex> vList6 = TestFrame.toArrayList(new Vertex[]{b, bottom, c});
        ArrayList<Vertex> vList7 = TestFrame.toArrayList(new Vertex[]{c, bottom, d});
        ArrayList<Vertex> vList8 = TestFrame.toArrayList(new Vertex[]{d, bottom, a});

        MyPolygon tri1 = new MyPolygon(Color.red, vList1);
        MyPolygon tri2 = new MyPolygon(Color.orange, vList2);
        MyPolygon tri3 = new MyPolygon(Color.yellow, vList3);
        MyPolygon tri4 = new MyPolygon(Color.green, vList4);
        MyPolygon tri5 = new MyPolygon(Color.blue, vList5);
        MyPolygon tri6 = new MyPolygon(Color.magenta, vList6);
        MyPolygon tri7 = new MyPolygon(Color.pink, vList7);
        MyPolygon tri8 = new MyPolygon(Color.LIGHT_GRAY, vList8);
        ArrayList<MyPolygon> faces = new ArrayList<MyPolygon>();
        faces.add(tri1);
        faces.add(tri2);
        faces.add(tri3);
        faces.add(tri4);
        faces.add(tri5);
        faces.add(tri6);
        faces.add(tri7);
        faces.add(tri8);
        MyEntity diamond = new MyEntity(origin.getX(), origin.getY(), origin.getZ(), faces);
        return diamond;
        //Vertex g = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() - (z / 2), "g");
        //Vertex h = new Vertex(center.getX() + (x / 2), center.getY() + (y / 2), center.getZ() + (z / 2), "h");
    }

    public static ArrayList<Vertex> toArrayList(Vertex[] v) {
        ArrayList<Vertex> temp = new ArrayList<Vertex>();
        for (int i = 0; i < v.length; i++) {
            temp.add(v[i]);
        }
        return temp;
    }

    public void splitFace(int x, int y, MyPolygon p) {
        ArrayList<Vertex> store = (ArrayList<Vertex>) entity.getAllVertices().clone();
        Vertex newV = new Vertex(0, 0, 0, ("" + (char) ((((int) 'a')) + entity.getAllVertices().size())));
        //System.out.println(p.getAveDepth());
        newV.convertFromScreenCoord(x, y, p.getAveDepth(), entity.getOriX(), entity.getOriY(), entity.getOriZ());
        Vertex lt = new Vertex(newV.getTX(), newV.getTY(), newV.getTZ());
        lt.setTX(newV.getTX());
        lt.setTY(newV.getTY());
        lt.setTZ(newV.getTZ());
//        System.out.println(newV.getLString());
//        System.out.println(newV.getTString());
//        System.out.println(newV.getWString());
//        System.out.println();
        lt.rotate(-rotXSum, -rotYSum, -rotZSum);
        newV.setLX(lt.getTX());
        newV.setLY(lt.getTY());
        newV.setLZ(lt.getTZ());
        //System.out.println(newV.getLString());
        //System.out.println(newV.getTString());
        //System.out.println(newV.getWString());
        ArrayList<Vertex> vert = (ArrayList<Vertex>) p.getVertices().clone();
        ArrayList<MyPolygon> newFaces = new ArrayList<MyPolygon>();
        for (int i = 0; i < vert.size(); i++) {
            int next = i + 1;
            next %= vert.size();
            ArrayList<Vertex> temp = new ArrayList<Vertex>();
            temp.add(vert.get(i));
            temp.add(vert.get(next));
            temp.add(newV);
            MyPolygon newp = new MyPolygon(p.getColor(), temp);
            newFaces.add(newp);
        }
        //ArrayList<MyPolygon> entF=( ArrayList<MyPolygon>)entity.getFaces().clone();
        //entF.remove(p);
        //entF.addAll(newFaces);
        //entity.setFaces(entF);

        //entity.getFaces().remove(p);

        entity.getFaces().addAll(newFaces);

        vertexList.setListData(entity.getAllVertices().toArray());
        polygonList.setListData(entity.getFaces().toArray());

    }

    public void init() //New Function!
    {
        image = createImage(2000, 2000);
        offScreen = image.getGraphics();
    }

    public void start() {
        if (animation == null) {
            animation = new Thread(this, "ThreadDemo");
            animation.start();
        }
    }

    public void run() {
        Thread myThread = Thread.currentThread();
        double divAmount = 100.0;
        while (animation == myThread) {


            if (upPressed) {
                //System.out.println("up Pressed");
                if (isAltDown) {
                    rotX += (1.0);
                } else {
                    shiftY += (1.0 / divAmount);
                }
            }
            if (downPressed) {
                if (isAltDown) {
                    rotX -= (1.0);
                } else {
                    shiftY -= (1.0 / divAmount);
                }
            }
            if (leftPressed) {
                if (isAltDown) {
                    rotY -= (1.0);
                } else {
                    shiftX -= (1.0 / divAmount);
                }
            }
            if (rightPressed) {
                if (isAltDown) {
                    rotY += (1);
                } else {
                    shiftX += (1.0 / divAmount);
                }
            }
            if (plusPressed) {
                if (isAltDown) {
                    rotZ += (1.0);
                } else {
                    shiftZ += (1.0 / divAmount);
                }
            }
            if (minusPressed) {
                if (isAltDown) {
                    rotZ -= (1.0);
                } else {
                    shiftZ -= (1.0 / divAmount);
                }
            }
            //System.out.println(shiftX+","+shiftY+","+shiftZ);
            repaint();
            try {
                Thread.sleep(15);
            } catch (InterruptedException e) {
            }
        }
    }

    public void update(Graphics g) //New Function!
    {
        jLayeredPane1.grabFocus();
        paint(g);
    }

    public void stop() {
        animation = null;
    }

    private Matrix translate(double x, double y, double z) {
        double[][] temp = new double[][]{
            {1, 0, 0, x},
            {0, 1, 0, y},
            {0, 0, 1, z},
            {0, 0, 0, 1}
        };
        Matrix m = new Matrix(temp);
        //m=m.transpose();
        return m;
    }

    private Matrix rotateX(double rot) {
        double[][] temp = new double[][]{
            {1, 0, 0, 0},
            {0, Math.cos(rot), Math.sin(rot), 0},
            {0, (-1 * Math.sin(rot)), Math.cos(rot), 0},
            {0, 0, 0, 1}
        };
        /*temp[0][0]=1;
         temp[0][1]=0;
         temp[0][2]=0;
         temp[0][3]=0;
         temp[1][0]=0;
         temp[1][1]=Math.cos(rot);
         temp[1][2]=Math.sin(rot);
         temp[1][3]=0;
         temp[2][0]=0;
         temp[2][1]=-1*Math.sin(rot);
         temp[2][2]=Math.cos(rot);
         temp[2][3]=0;
         temp[3][0]=0;
         temp[3][1]=0;
         temp[3][2]=0;
         temp[3][3]=1;*/
        Matrix m = new Matrix(temp);
        //System.out.println("not transposed");
        //m=m.transpose();
        return m;


    }

    private Matrix rotateY(double rot) {
        double[][] temp = new double[][]{
            {Math.cos(rot), 0, (-1 * Math.sin(rot)), 0},
            {0, 1, 0, 0},
            {Math.sin(rot), 0, Math.cos(rot), 0},
            {0, 0, 0, 1}
        };
        /*temp[0][0]=1;
         temp[0][1]=0;
         temp[0][2]=0;
         temp[0][3]=0;
         temp[1][0]=0;
         temp[1][1]=Math.cos(rot);
         temp[1][2]=Math.sin(rot);
         temp[1][3]=0;
         temp[2][0]=0;
         temp[2][1]=-1*Math.sin(rot);
         temp[2][2]=Math.cos(rot);
         temp[2][3]=0;
         temp[3][0]=0;
         temp[3][1]=0;
         temp[3][2]=0;
         temp[3][3]=1;*/
        Matrix m = new Matrix(temp);
        m = m.transpose();
        return m;


    }

    private Matrix rotateZ(double rot) {
        double[][] temp = new double[][]{
            {Math.cos(rot), Math.sin(rot), 0, 0},
            {(-1 * Math.sin(rot)), Math.cos(rot), 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1}
        };

        Matrix m = new Matrix(temp);
        m = m.transpose();
        return m;


    }

    public static Matrix rotate(double rotX, double rotY, double rotZ, Matrix inputMatrix) {
        Matrix rotMatrix = null;
        Matrix tempMatrix1 = null;
        Matrix tempMatrix2 = null;
        Matrix tempMatrix3 = null;
        double xSin = Math.sin(rotX);
        double ySin = Math.sin(rotY);
        double zSin = Math.sin(rotZ);
        double xCos = Math.cos(rotX);
        double yCos = Math.cos(rotY);
        double zCos = Math.cos(rotZ);

        rotMatrix = new Matrix(new double[][]{
            {yCos, 0, -ySin, 0},
            {0, 1, 0, 0},
            {ySin, 0, yCos, 0},
            {0, 0, 0, 1}
        });
        tempMatrix1 = rotMatrix.times(inputMatrix);
        rotMatrix = new Matrix(new double[][]{
            {1, 0, 0, 0},
            {0, xCos, xSin, 0},
            {0, -xSin, xCos, 0},
            {0, 0, 0, 1}
        });
        tempMatrix2 = rotMatrix.times(tempMatrix1);
        rotMatrix = new Matrix(new double[][]{
            {zCos, zSin, 0, 0},
            {-zSin, zCos, 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1}
        });
        tempMatrix3 = rotMatrix.times(tempMatrix2);


        return tempMatrix3;
    }

    private Matrix scale(double x, double y, double z) {
        double[][] temp = new double[][]{
            {x, 0, 0, 0},
            {0, y, 0, 0},
            {0, 0, z, 0},
            {0, 0, 0, 1}
        };

        Matrix m = new Matrix(temp);
        m = m.transpose();
        return m;
    }

    private boolean isBackFacing(Point3D v0, Point3D v1, Point3D v2) {
        double cullMe, x1, x2, x3, y1, y2, y3, z1, z2, z3;
        x1 = (float) v0.getX();
        x2 = (float) v1.getX();
        x3 = (float) v2.getX();
        y1 = (float) v0.getY();
        y2 = (float) v1.getY();
        y3 = (float) v2.getY();
        z1 = (float) v0.getZ();
        z2 = (float) v1.getZ();
        z3 = (float) v2.getZ();
        cullMe = x3 * ((z1 * y2) - (y1 * z2))
                + y3 * ((x1 * z2) - (z1 * x2))
                + z3 * ((y1 * x2) - (x1 * y2));
        return (cullMe < 0.0);
    }
    private boolean shouldPause = false;

    public void paint(Graphics g) {
        jLayeredPane1.grabFocus();
        myPos = new Point3D(shiftX, shiftY, shiftZ);
        offScreen.clearRect(0, 0, 10000, 10000);
        Image tempImage = createImage(2000, 2000);
        Graphics tmp = tempImage.getGraphics();
        super.paint(tmp);
        offScreen.drawImage(tempImage, 0, 0, this);



        /*shiftXLabel.setText("shiftX: " + shiftX);
         shiftYLabel.setText("shiftY: " + shiftY);
         shiftZLabel.setText("shiftZ: " + shiftZ);
         rotXLabel.setText("rotX: " + rotX);
         rotYLabel.setText("rotY: " + rotY);
         rotZLabel.setText("rotZ: " + rotZ);*/
        //entity.setToDefault();
        Graphics tempG = offScreen;
        entity.rotate(Math.toRadians(rotX), Math.toRadians(rotY), Math.toRadians(rotZ));
        rotXSum += rotX;
        rotYSum += rotY;
        rotZSum += rotZ;
        rotX = 0;
        rotY = 0;
        rotZ = 0;
        /*entity.translate(shiftX, shiftY, shiftZ);
         shiftX=0;
         shiftY=0;
         shiftZ=0;*/
        entity.setOrigin(shiftX, shiftY, shiftZ);
        entity.toWorldCoordinates();
        //entity.transform();
        entity.draw(tempG);
        if (isAltDown || isShiftPressed) {
            entity.drawNodes(tempG);
        }
        if (displayPositions) {
            entity.drawPositions(tempG);
        }

        /*Matrix concat = Matrix.identity(4);

         //concat = (rotateZ(Math.toRadians(rotZ))).times(concat);
         /*if(shouldPause)
         {
         System.out.println("RotZ");
         concat.times(nodeMatrix).show();
         System.out.println();
         //scanner.nextLine();
         }*/
        //concat = (rotateX(Math.toRadians(rotX))).times(concat);
        /*if(shouldPause)
         {
         System.out.println("RotX");
         concat.times(nodeMatrix).show();
         System.out.println();
         //scanner.nextLine();
         }*/
        //concat = (rotateY(Math.toRadians(rotY))).times(concat);
        /*if(shouldPause)
         {
         System.out.println("RotY");
         concat.times(nodeMatrix).show();
         System.out.println();
         //scanner.nextLine();
         }*/
        //concat=concat.times(scale(100,100,100));
        /*if (shouldPause) {
         System.out.println("Scale");
         concat.times(nodeMatrix).show();
         System.out.println();
         //scanner.nextLine();
         }
         concat = concat.times(translate(shiftX, shiftY, shiftZ));
         /*if(shouldPause)
         {
            
            
         System.out.println("translate");
         concat.times(nodeMatrix).show();
         System.out.println();
         //scanner.nextLine();
         }*/
        //concat.show();
        //modifiedMatrix = concat.times(nodeMatrix);
        //modifiedMatrix=rotate(Math.toRadians(rotX),Math.toRadians(rotY),Math.toRadians(rotZ),nodeMatrix);
        //modifiedMatrix=concat.times(modifiedMatrix);
        //modifiedMatrix.show();
        /*System.out.println();
         super.paint(offScreen);
         displayNodes.clear();
         /*for(Point3D p:nodes)
         {
         double[][] temp=new double[4][0];
            
         temp[0][0]=p.getX();
         temp[1][0]=p.getY();
         temp[2][0]=p.getZ();
         temp[3][0]=1;
                    
         displayNodes.add(new Point(350+(int)(((myPos.getX()-p.getX())*100)/(myPos.getZ()-p.getZ())),350+(int)(((myPos.getY()-p.getY())*100)/(myPos.getZ()-p.getZ()))));
            
         /*g.fillOval(350+(int)(((p.getX()-myPos.getX())*100)/(p.getZ()-myPos.getZ())), 
         350+(int)(((p.getY()-myPos.getY())*100)/(p.getZ()-myPos.getZ())), 10, 10);*/
        //}
        /*double[][] array = modifiedMatrix.getArray();
         for (int i = 0; i < array[0].length; i++) {
         Point3D p = new Point3D(array[0][i], array[1][i], array[2][i]);
         double wx = p.getX();
         double wy = p.getY();
         double wz = p.getZ();
         double c = 200;
         double sx = wx * c / wz;
         double sy = wy * c / wz;
         if (shouldPause) {
         System.out.println(p);
         }
         //displayNodes.add(new Point(350+(int)(((myPos.getX()-p.getX())*100)/(myPos.getZ()-p.getZ())),350+(int)(((myPos.getY()-p.getY())*100)/(myPos.getZ()-p.getZ()))));
         displayNodes.add(new Point((int) sx, (int) sy));
         }
         if (shouldPause) {
         System.out.println();
         shouldPause = false;
         }
         //System.out.println(offScreen==null);
         for (int i = 0; i < displayNodes.size(); i++) {
         //System.out.println((int)displayNodes.get(i).getX()+","+ (int)displayNodes.get(i).getY());
         for (int j = 0; j < displayNodes.size(); j++) {
         if (j != i) {
         offScreen.drawLine(350 + (int) displayNodes.get(i).getX(), 350 + (int) displayNodes.get(i).getY(), 350 + (int) displayNodes.get(j).getX(), 350 + (int) displayNodes.get(j).getY());
         }
         }
         /*for(int j=i+1; j<displayNodes.size(); j++)
         {
         for(int a=j+1; a<displayNodes.size(); a++)
         {
         for(int b=a+1; b<displayNodes.size(); b++)
         {
         int[] nodeXs=new int[4];
         int[] nodeYs=new int[4];
         nodeXs[0]=(int)displayNodes.get(i).getX();
         nodeXs[1]=(int)displayNodes.get(j).getX();
         nodeXs[2]=(int)displayNodes.get(a).getX();
         nodeXs[3]=(int)displayNodes.get(b).getX();
         nodeYs[0]=(int)displayNodes.get(i).getY();
         nodeYs[1]=(int)displayNodes.get(j).getY();
         nodeYs[2]=(int)displayNodes.get(a).getY();
         nodeYs[3]=(int)displayNodes.get(b).getY();
         Polygon p=new Polygon(nodeXs, nodeYs, 4);
         g.setColor(Color.BLACK);
         //g.fillPolygon(p);
         //g.setColor(Color.yellow);
         g.drawPolygon(p);
                        
         }
         }
                    
         //g.drawLine((int)displayNodes.get(i).getX(), (int)displayNodes.get(i).getY(), (int)displayNodes.get(j).getX(), (int)displayNodes.get(j).getY());
                
         }*/
        //}
        //g.drawLine(, WIDTH, WIDTH, WIDTH);
        //shapeChooserFrame.paint(offScreen);
        //super.paint(offScreen);

        /*jPanel1.move(0,22);
         jPanel1.paint(offScreen);
         jPanel1.move(0,-22);*/
        //super.paint(offScreen);
        offScreen.translate(0, 22);
        jLayeredPane1.paint(offScreen);
        offScreen.translate(0, -22);
        g.drawImage(image, 0, 0, this);

        //Image tempImage = createImage(2000, 2000);


    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane3 = new javax.swing.JScrollPane();
        vertexList1 = new javax.swing.JList();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        polygonList = new javax.swing.JList();
        jButton2 = new javax.swing.JButton();
        shapeChooserFrame = new javax.swing.JInternalFrame();
        jLabel3 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jButton4 = new javax.swing.JButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jButton3 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        vertexList = new javax.swing.JList();
        colorChooserFrame = new javax.swing.JInternalFrame();
        jColorChooser1 = new javax.swing.JColorChooser();
        setColorButton = new javax.swing.JButton();
        cancelColorButton = new javax.swing.JButton();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();

        vertexList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(vertexList1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jLayeredPane1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                jLayeredPane1MouseWheelMoved(evt);
            }
        });
        jLayeredPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLayeredPane1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLayeredPane1MouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLayeredPane1MouseClicked(evt);
            }
        });
        jLayeredPane1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLayeredPane1MouseDragged(evt);
            }
        });
        jLayeredPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLayeredPane1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jLayeredPane1KeyReleased(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setBounds(650, 10, 12, 540);
        jLayeredPane1.add(jSeparator1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jSeparator3.setBounds(670, 300, 184, 12);
        jLayeredPane1.add(jSeparator3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel1.setText("Vertices");
        jLabel1.setBounds(740, 20, 50, 16);
        jLayeredPane1.add(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel2.setText("Polygons");
        jLabel2.setBounds(730, 320, 57, 16);
        jLayeredPane1.add(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        polygonList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(polygonList);

        jScrollPane2.setBounds(680, 337, 220, 210);
        jLayeredPane1.add(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton2.setText("Add Vertex");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.setBounds(750, 240, 113, 29);
        jLayeredPane1.add(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        shapeChooserFrame.setClosable(true);
        shapeChooserFrame.setResizable(true);
        shapeChooserFrame.setOpaque(true);
        shapeChooserFrame.setVisible(true);
        shapeChooserFrame.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                shapeChooserFrameKeyPressed(evt);
            }
        });

        jLabel3.setText("Choose A Basic Shape");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setMnemonic('0');
        jRadioButton1.setText("Cube");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setMnemonic('1');
        jRadioButton2.setText("Pyramid");

        buttonGroup1.add(jRadioButton3);
        jRadioButton3.setMnemonic('2');
        jRadioButton3.setText("Diamond");

        jButton4.setText("GO!");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton4);
        jRadioButton4.setMnemonic('3');
        jRadioButton4.setText("FlatPlane");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout shapeChooserFrameLayout = new org.jdesktop.layout.GroupLayout(shapeChooserFrame.getContentPane());
        shapeChooserFrame.getContentPane().setLayout(shapeChooserFrameLayout);
        shapeChooserFrameLayout.setHorizontalGroup(
            shapeChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(shapeChooserFrameLayout.createSequentialGroup()
                .add(shapeChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(shapeChooserFrameLayout.createSequentialGroup()
                        .add(79, 79, 79)
                        .add(jLabel3))
                    .add(shapeChooserFrameLayout.createSequentialGroup()
                        .add(19, 19, 19)
                        .add(shapeChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jRadioButton2)
                            .add(jRadioButton1)
                            .add(jRadioButton3)
                            .add(jRadioButton4))))
                .addContainerGap(157, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, shapeChooserFrameLayout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(jButton4)
                .add(28, 28, 28))
        );
        shapeChooserFrameLayout.setVerticalGroup(
            shapeChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(shapeChooserFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel3)
                .add(18, 18, 18)
                .add(jRadioButton1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioButton2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioButton3)
                .add(shapeChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(shapeChooserFrameLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jButton4)
                        .add(31, 31, 31))
                    .add(shapeChooserFrameLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jRadioButton4)
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        shapeChooserFrame.setBounds(170, 190, 397, 233);
        jLayeredPane1.add(shapeChooserFrame, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton3.setText("New Shape");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.setBounds(10, 560, 112, 29);
        jLayeredPane1.add(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton1.setText("Add Polygon");
        jButton1.setBounds(750, 560, 123, 29);
        jLayeredPane1.add(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        vertexList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(vertexList);

        jScrollPane1.setBounds(680, 40, 220, 190);
        jLayeredPane1.add(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        colorChooserFrame.setVisible(false);

        setColorButton.setText("Set");
        setColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setColorButtonActionPerformed(evt);
            }
        });

        cancelColorButton.setText("Cancel");
        cancelColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelColorButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout colorChooserFrameLayout = new org.jdesktop.layout.GroupLayout(colorChooserFrame.getContentPane());
        colorChooserFrame.getContentPane().setLayout(colorChooserFrameLayout);
        colorChooserFrameLayout.setHorizontalGroup(
            colorChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(colorChooserFrameLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(colorChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jColorChooser1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, colorChooserFrameLayout.createSequentialGroup()
                        .add(cancelColorButton)
                        .add(27, 27, 27)
                        .add(setColorButton)))
                .addContainerGap())
        );
        colorChooserFrameLayout.setVerticalGroup(
            colorChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, colorChooserFrameLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jColorChooser1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 269, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(colorChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(setColorButton)
                    .add(cancelColorButton))
                .addContainerGap())
        );

        colorChooserFrame.setBounds(130, 140, 480, 362);
        jLayeredPane1.add(colorChooserFrame, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jInternalFrame1.setClosable(true);
        jInternalFrame1.setIconifiable(true);
        jInternalFrame1.setResizable(true);
        jInternalFrame1.setVisible(true);

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "ALT: drag points", "Control: Split face", "\"C\": change color", " " };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList1);

        org.jdesktop.layout.GroupLayout jInternalFrame1Layout = new org.jdesktop.layout.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jInternalFrame1Layout.createSequentialGroup()
                .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                .addContainerGap())
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jInternalFrame1Layout.createSequentialGroup()
                .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                .addContainerGap())
        );

        jInternalFrame1.setBounds(0, 0, 150, 170);
        jLayeredPane1.add(jInternalFrame1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jLayeredPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 912, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jLayeredPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 590, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 6, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        keyPressed(evt);
    }//GEN-LAST:event_formKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
        keyReleased(evt);
    }//GEN-LAST:event_formKeyReleased

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        shapeChooserFrame.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:

        int num = buttonGroup1.getSelection().getMnemonic();
        num -= 48;
        //System.out.println(num);
        if (num == 0) {
            entity = createCubeAt(new Point3D(0, 0, 0), 1, 1, 1);
        } else if (num == 1) {
            entity = createPyramidAt(new Point3D(0, 0, 0), 1, 1, 1);
        } else if (num == 2) {
            entity = createDiamondAt(new Point3D(0, 0, 0), 1, 1, 1);
        } else if (num == 3) {
            entity = createPlaneAt(new Point3D(0, 0, 0), 1, 1);
        }
        entity.setToDefault();
        vertexList.setListData(entity.getAllVertices().toArray());
        polygonList.setListData(entity.getFaces().toArray());
        this.stop();
        init();
        start();
        shapeChooserFrame.setVisible(false);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        vertexList.setListData(entity.getAllVertices().toArray());

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jLayeredPane1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLayeredPane1KeyReleased
        // TODO add your handling code here:
        keyReleased(evt);
    }//GEN-LAST:event_jLayeredPane1KeyReleased

    private void jLayeredPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLayeredPane1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLayeredPane1MouseClicked

    private void jLayeredPane1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLayeredPane1MouseDragged
        // TODO add your handling code here:
        mouseDragged(evt);
    }//GEN-LAST:event_jLayeredPane1MouseDragged

    private void jLayeredPane1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLayeredPane1MousePressed
        // TODO add your handling code here:
        mousePressed(evt);
    }//GEN-LAST:event_jLayeredPane1MousePressed

    private void jLayeredPane1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLayeredPane1MouseReleased
        // TODO add your handling code here:
        mouseReleased(evt);
    }//GEN-LAST:event_jLayeredPane1MouseReleased

    private void jLayeredPane1MouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_jLayeredPane1MouseWheelMoved
        // TODO add your handling code here:
        mouseWheelMoved(evt);
    }//GEN-LAST:event_jLayeredPane1MouseWheelMoved

    private void jLayeredPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLayeredPane1KeyPressed
        // TODO add your handling code here:

        keyPressed(evt);
    }//GEN-LAST:event_jLayeredPane1KeyPressed

    private void shapeChooserFrameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_shapeChooserFrameKeyPressed
        // TODO add your handling code here:
        //System.out.println("hi");
    }//GEN-LAST:event_shapeChooserFrameKeyPressed

    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void setColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setColorButtonActionPerformed
        // TODO add your handling code here:
        colorChooserFrame.setVisible(false);
        selectedPolygon.setColor(jColorChooser1.getColor());
        selectedPolygon = null;
        isChoosingColor = false;
    }//GEN-LAST:event_setColorButtonActionPerformed

    private void cancelColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelColorButtonActionPerformed
        // TODO add your handling code here:
        colorChooserFrame.setVisible(false);
        selectedPolygon = null;
        isChoosingColor = false;
    }//GEN-LAST:event_cancelColorButtonActionPerformed
    private Point clickPoint;
    private Vertex selectedV;

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TestFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cancelColorButton;
    private javax.swing.JInternalFrame colorChooserFrame;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JList jList1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JList polygonList;
    private javax.swing.JButton setColorButton;
    private javax.swing.JInternalFrame shapeChooserFrame;
    private javax.swing.JList vertexList;
    private javax.swing.JList vertexList1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private boolean controlPressed = false;
    private boolean displayPositions = false;
    private boolean isDeleting = false;
    //private ArrayList<Vertex> simulSelectedVertices;

    public void keyPressed(KeyEvent evt) {
        isShiftPressed = evt.isShiftDown();
        isAltDown = evt.isAltDown();

        controlPressed = evt.isControlDown();
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (shouldPause) {
                shouldPause = false;
            } else {
                shouldPause = true;
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            isDeleting = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            //System.out.println("check");
            upPressed = true;

        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            downPressed = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            rightPressed = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            leftPressed = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_Q) {
            plusPressed = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_E) {
            minusPressed = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            shiftX = 0;
            shiftY = 0;
            shiftZ = 0;
            rotX = 0;
            rotY = 0;
            rotZ = 0;
        }
        if (evt.getKeyCode() == KeyEvent.VK_P) {
            if (displayPositions) {
                displayPositions = false;

            } else {
                displayPositions = true;
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_C) {
            isChoosingColor = true;
        }
        if (evt.getKeyCode() == KeyEvent.VK_M && simulSelectedVertices.size() > 0) {

            Vertex mid = simulSelectedVertices.get(0);
            for (int i = 1; i < simulSelectedVertices.size(); i++) {
                mid = mid.midPointVertex(simulSelectedVertices.get(i));
            }

            for (Vertex v : simulSelectedVertices) {
                v.copy(mid);
            }


        }
        repaint();
    }
    private boolean isChoosingColor = false;
    private boolean isShiftPressed = false;
    private MyPolygon selectedPolygon = null;

    private MyPolygon getPolygonAt(int x, int y) {
        ArrayList<MyPolygon> faces = (ArrayList<MyPolygon>) entity.getFaces().clone();
        Collections.sort(faces);
        MyPolygon p = null;
        for (MyPolygon f : faces) {
            //System.out.println(f.doesContain(evt.getX() - 350, pressedY));
            if (f.doesContain(x, y)) {
                p = f;
            }
        }
        return p;
    }

    @Override
    public void keyReleased(KeyEvent evt) {
        isShiftPressed = evt.isShiftDown();
        isAltDown = evt.isAltDown();
        controlPressed = evt.isControlDown();
        if (evt.getKeyCode() == KeyEvent.VK_UP) {

            upPressed = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            downPressed = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            rightPressed = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            leftPressed = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_Q) {
            plusPressed = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_E) {
            minusPressed = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_C) {
            isChoosingColor = true;
        } else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            isDeleting = false;
        } else if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            for (Vertex v : simulSelectedVertices) {
                v.setIsSelected(false);
            }
            simulSelectedVertices.clear();
        }
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        if (isDeleting) {
            //System.out.println("deleting");
            int pressedY = evt.getY() - 350 + 20;
            MyPolygon p = getPolygonAt(evt.getX() - 350, pressedY);
            ArrayList<MyPolygon> ps = entity.getFaces();
            ps.remove(p);
            entity.setFaces(ps);
        }
        if (isShiftPressed) {
            ArrayList<Vertex> storage = entity.getVertexAt(evt.getX() - 350, evt.getY() - 328, 7);
            for (int i = 0; i < storage.size(); i++) {
                selectedV = storage.get(i);
                if (selectedV != null) {

                    selectedV.setIsSelected(true);
                    if (!simulSelectedVertices.contains(selectedV)) {
                        //simulSelectedVertices.remove(selectedV);
                        simulSelectedVertices.add(selectedV);
                        //selectedV.setIsSelected(false);
                    } 

                }
            }
        }
        if (isAltDown) {
            //System.out.println("pressed");
            simulSelectedVertices.clear();

            simulSelectedVertices.addAll(entity.getVertexAt(evt.getX() - 350, evt.getY() - 328, 7));
            selectedV = simulSelectedVertices.get(simulSelectedVertices.size() - 1);
            if (simulSelectedVertices.size() > 0) {
                for (Vertex v : simulSelectedVertices) {
                    v.setIsSelected(true);
                }

            }
        }
        if (controlPressed) {
            int pressedY = evt.getY() - 350 + 20;

            MyPolygon p = getPolygonAt(evt.getX() - 350, pressedY);
            if (p != null) {
                splitFace(evt.getX() - 350, pressedY, p);
            }
        }
        if (isChoosingColor) {
            int pressedY = evt.getY() - 350 + 20;
            selectedPolygon = getPolygonAt(evt.getX() - 350, pressedY);
            colorChooserFrame.setVisible(true);

        }
        clickPoint = evt.getPoint();
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //System.out.println("released");
        if (simulSelectedVertices.size() > 0 && !isShiftPressed) {
            for (Vertex v : simulSelectedVertices) {
                //if()
                //{
                v.setIsSelected(false);

                //}
            }
            simulSelectedVertices.clear();

        }


        repaint();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseDragged(MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            if (simulSelectedVertices.size() > 0) {
                for (Vertex v : simulSelectedVertices) {
                    double distX = (selectedV.getSX()) - v.getSX();
                    double distY = (selectedV.getSY()) - v.getSY();
                    v.convertFromScreenCoord(clickPoint.getX() - 345 - distX, clickPoint.getY() - 320 - distY, v.getWZ(), entity.getOriX(), entity.getOriY(), entity.getOriZ());
                    //selectedV.setLX(selectedV.getLX() - (clickPoint.getX() - evt.getX()) / 500.0);
                    //selectedV.setLY(selectedV.getLY() - (clickPoint.getY() - evt.getY()) / 500.0);
                }
            } else {
                Vertex tempOri = new Vertex(0, 0, 0);
                tempOri.convertFromScreenCoord(evt.getX(), evt.getY(), entity.getOriZ(), 0, 0, 0);
                entity.setOrigin(tempOri.getTX(), tempOri.getTY(), tempOri.getTZ());
                shiftX -= (clickPoint.getX() - evt.getX()) / 500.0;
                shiftY -= (clickPoint.getY() - evt.getY()) / 500.0;
            }
        } else {
            rotY += (clickPoint.getX() - evt.getX()) / 2.0;
            rotX -= (clickPoint.getY() - evt.getY()) / 2.0;
        }
        clickPoint = evt.getPoint();
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent evt) {
        if (simulSelectedVertices.size() > 0) {
            for (Vertex v : simulSelectedVertices) {
                v.setTZ(v.getTZ() - (evt.getWheelRotation() / 100.0));
                //selectedV.setLX(selectedV.getLX() - (clickPoint.getX() - evt.getX()) / 500.0);
                //selectedV.setLY(selectedV.getLY() - (clickPoint.getY() - evt.getY()) / 500.0);
            }
            //selectedV.setTZ(selectedV.getTZ() - (evt.getWheelRotation() / 100.0));
        } else if (isAltDown) {
            rotZ += (evt.getWheelRotation() / 10.0);
        } else {
            shiftZ += (evt.getWheelRotation() / 100.0);
        }
    }
}
