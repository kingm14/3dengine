/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dengine;

import java.awt.Graphics;
import java.awt.Point;
import java.util.*;

/**
 *
 * @author kingm14
 */
public class MyEntity {
    private ArrayList<MyPolygon> faces;
    
    public double oriX,oriY,oriZ;
    double rotXSum;
    double rotYSum;
    double rotZSum;
    double shiftXSum;
    double shiftYSum;
    double shiftZSum;
    public MyEntity(double x, double y, double z)
    {
        oriX=x;
        oriY=y;
        oriZ=z;
        rotXSum=0;
        rotYSum=0;
        rotZSum=0;
        shiftXSum=0;
        shiftYSum=0;
        shiftZSum=0;
        faces=new  ArrayList<MyPolygon>();
    }
    public MyEntity(double x, double y, double z, ArrayList<MyPolygon> f)
    {
        oriX=x;
        oriY=y;
        oriZ=z;
        rotXSum=0;
        rotYSum=0;
        rotZSum=0;
        shiftXSum=0;
        shiftYSum=0;
        shiftZSum=0;
        faces=f;
    }
    public double getRotXSum()
    {
        return rotXSum;
    }
    public double getRotYSum()
    {
        return rotYSum;
    }
    public double getRotZSum()
    {
        return rotZSum;
    }
    public void setFaces(ArrayList<MyPolygon> newFaces)
    {
        faces=newFaces;
    }
    public void setOrigin(double x, double y, double z)
    {
        oriX=x;
        oriY=y;
        oriZ=z;
    }
    public ArrayList<MyPolygon> getFaces()
    {
        return faces;
    }
    
    public void rotateOffCenter(double rotX, double rotY,double rotZ)
    {
        rotXSum+=rotX;
        rotYSum+=rotY;
        rotZSum+=rotZ;
        for(MyPolygon f:faces)
        {
            f.rotate(rotX, rotY, rotZ);
        }
        for(MyPolygon f:faces)
        {
            f.resetVertexRotBool();
        }
        
    }
    public void rotate(double rotX, double rotY, double rotZ)
    {
        //this.setToDefault();
        rotXSum+=rotX;
        rotYSum+=rotY;
        rotZSum+=rotZ;
        //translate(shiftXSum,shiftYSum,shiftZSum);
        
        for(MyPolygon p:faces)
        {
            //System.out.println(v);
            if(p!=null)
                p.rotate(rotX, rotY, rotZ);
        }
        for(MyPolygon f:faces)
        {
            f.resetVertexRotBool();
        }
        /*for(MyPolygon f:faces)
        {
            f.translate(shiftXSum,shiftYSum,shiftZSum);
        }*/
    }
    public void translate(double x, double y,double z)
    {
        shiftXSum+=x;
        shiftYSum+=y;
        shiftZSum+=z;
        //System.out.println(shiftXSum+","+shiftYSum+","+shiftZSum);
        for(MyPolygon f:faces)
        {
            f.translate(x, y, z);
        }
    }
    public void setToDefault()
    {
        for(MyPolygon f:faces)
        {
            f.setToDefault();
            
        }
        for(MyPolygon p:faces)
        {
            //System.out.println(v);
            if(p!=null)
                p.rotate(rotXSum, rotYSum, rotZSum);
            
        }
        for(MyPolygon f:faces)
        {
            f.resetVertexRotBool();
        }
         for(MyPolygon f:faces)
        {
            f.translate(shiftXSum,shiftYSum,shiftZSum);
        }
         
        //rotate(rotXSum,rotYSum,rotZSum);
        //translate(shiftXSum,shiftYSum,shiftZSum);
    }
    public double getOriX()
    {
        return oriX;
    }
    public double getOriY()
    {
        return oriY;
    }
    public double getOriZ()
    {
        return oriZ;
    }
    public void transform()
    {
        
        /*Vertex theVertex;
        MyPolygon thePolygon;
        int numOfFaces=faces.size();
        for(int p=0; p<numOfFaces; p++)
        {
            
            thePolygon=faces.get(p);
            
            for(int v=0; v<thePolygon.getVertices().size();v++)
            {
                theVertex=thePolygon.getVertices().get(v);
                if(theVertex!=null)
                {
                theVertex.setTX(theVertex.getLX());
                theVertex.setTY(theVertex.getLY());
                theVertex.setTZ(theVertex.getLZ());
                theVertex.tt=1.0;
                }
            }
            
        }*/
    }
    public void toWorldCoordinates()
    {
        
        Vertex theVertex;
        MyPolygon thePolygon;
        int numOfFaces=faces.size();
        for(int p=0; p<numOfFaces; p++)
        {
            
            thePolygon=faces.get(p);
            
            for(int v=0; v<thePolygon.getVertices().size();v++)
            {
                theVertex=thePolygon.getVertices().get(v);
                if(theVertex!=null)
                {
                theVertex.setWX(theVertex.getTX()+oriX);
                theVertex.setWY(theVertex.getTY()+oriY);
                theVertex.setWZ(theVertex.getTZ()+oriZ);
                //System.out.println("("+theVertex.getWX()+","+theVertex.getWY()+","+theVertex.getWZ()+")");
                }
                
            }
            
        }
    }
    public void draw(Graphics g)
    {
        Collections.sort(faces);
        for(MyPolygon p:faces)
        {
            if(p.isBackfacing())
            {
                p.draw(g);
            }
        }
        
        for(MyPolygon p:faces)
        {
            if(!p.isBackfacing())
            {
                p.draw(g);
            }
        }
    }
    public ArrayList<Vertex> getVertexAt(double x, double y, double error)
    {
        ArrayList<Vertex> retList=new ArrayList<Vertex>();
       
        
        for(MyPolygon p:faces)
        {
            Vertex ret=null;
            //if(!p.isBackfacing())
            {
                ArrayList<Vertex> storage=p.getVertexAt(x, y, error);
                if(retList.size()==0)
                {
                    retList.addAll(storage);
                }
                ret=retList.get(0);
                Vertex v=storage.get(0);
                Point p0=new Point((int)x,(int)y);
                Point p1=new Point((int)v.getSX(),(int)v.getSY());
                Point p2=new Point((int)ret.getSX(),(int)ret.getSY());
                if(p0.distance(p1)<p0.distance(p2))
                {
                    retList=storage;
                }
                else if(ret.getWX()==v.getWX()&&ret.getWY()==v.getWY()&&ret.getWZ()==v.getWZ())
                {
                    retList.addAll(storage);
                }
            }
        }
        Point p0=new Point((int)x,(int)y);
                
        Point p2=new Point((int)retList.get(0).getSX(),(int)retList.get(0).getSY());
        if(p2.distance(p0)>error)
        {
            retList.clear();
        }
        return retList;
    }
    /*public Vertex getVerticesAt(double x, double y, double error)
    {
        Vertex ret=null;
        
        for(MyPolygon p:faces)
        {
            //if(!p.isBackfacing())
            {
                
                if(ret==null)
                {
                    ret=p.getVertexAt(x, y, error);
                }
                Vertex v=p.getVertexAt(x, y, error);
                Point p0=new Point((int)x,(int)y);
                Point p1=new Point((int)v.getSX(),(int)v.getSY());
                Point p2=new Point((int)ret.getSX(),(int)ret.getSY());
                if(p0.distance(p1)<p0.distance(p2))
                {
                    ret=v;
                }
            }
        }
        return ret;
    }*/
    public Vertex getVertex(String s)
    {
        for(MyPolygon p:faces)
        {
            
                
                ArrayList<Vertex> temp=(p.getVertices());
                for(Vertex v: temp)
                {
                    if(v.getName().equals(s))
                    {
                        return v;
                    }
                }
            
        }
        return null;
    }
    public ArrayList<Vertex> getAllVertices()
    {
        ArrayList<Vertex> vertices=new ArrayList<Vertex>();
        for(MyPolygon p:faces)
        {
            //if(!p.isBackfacing())
            {
                
                ArrayList<Vertex> temp=(p.getVertices());
                for(Vertex v: temp)
                {
                    if(!vertices.contains(v))
                    {
                        vertices.add(v);
                    }
                }
            }
        }
        return vertices;
    }
    public void drawNodes(Graphics g)
    {
        for(MyPolygon p:faces)
        {
            //if(!p.isBackfacing())
            //{
                p.drawNodes(g);
            //}
        }
    }
    public void drawPositions(Graphics g)
    {
        for(MyPolygon p:faces)
        {
            if(!p.isBackfacing())
            {
                p.drawPositions(g);
            }
        }
    }
    /*public Matrix toMatrix()
    {
        ArrayList<ArrayList<Double>> matrix=new ArrayList<ArrayList<Double>>();
        for(int i=0; i<faces.size(); i++)
        {
            //double[][] matrix=new double[faces.size()][4];
            ArrayList<Vertex> vs=faces.get(i).getVertices();
            for(int j=0;j<vs.size(); j++)
            {
                ArrayList<Double>temp=new ArrayList<Double>();
                
                        }
        }
        return null;
    }*/

    
}
