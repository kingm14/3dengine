/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dengine;

import java.util.Scanner;

/**
 *
 * @author kingm14
 */
public class Vertex implements Comparable{
    public double lx,ly,lz;
    public double tx,ty,tz;
    public double wx,wy,wz;
    public double sx,sy;
    public double lt, tt, wt, st;
    private final double c=350.0;
    private boolean isSelected;
    private String name;
    private boolean beenRotated=false;
    public Vertex(double x, double y, double z)
    {
        lx=x;
        ly=y;
        lz=z;
        tx=0;
        ty=0;
        tz=0;
        wx=0;
        wy=0;
        wz=0;
        isSelected=false;
        name="";
        
    }
    public void copy(Vertex v)
    {
        lx=v.getLX();
        ly=v.getLY();
        lz=v.getLZ();
        
        tx=v.getTX();
        ty=v.getTY();
        tz=v.getTZ();
        
        wx=v.getWX();
        wy=v.getWY();
        wz=v.getWZ();
        name=v.getName();
        isSelected=false;
    }
    public Vertex(double x, double y, double z,String name)
    {
        lx=x;
        ly=y;
        lz=z;
        tx=0;
        ty=0;
        tz=0;
        wx=0;
        wy=0;
        wz=0;
        isSelected=false;
        this.name=name;
    }
    public boolean hasBeenRotated()
    {
        return beenRotated;
    }
    public void setBeenRotated(boolean b)
    {
        beenRotated=b;
    }
    public String toString()
    {
        String blank="";
        for(int i=0; i<getName().length()+1;i++)
        {
            blank+=" ";
        }
        
        String s= (getName()+getLString()+"\n"
                      +blank+getTString()+"\n"
                      +blank+getWString());
        
        return s;
    }
    public String getLString()
    {
        double xtemp=((int)(lx*100))/100.0;
        double ytemp=((int)(ly*100))/100.0;
        double ztemp=((int)(lz*100))/100.0;
        return ":L:("+xtemp+","+ytemp+","+ztemp+")";
    }
    public String getTString()
    {
        double xtemp=((int)(tx*100))/100.0;
        double ytemp=((int)(ty*100))/100.0;
        double ztemp=((int)(tz*100))/100.0;
        return ":T:("+xtemp+","+ytemp+","+ztemp+")";
    }
    public String getWString()
    {
        double xtemp=((int)(wx*100))/100.0;
        double ytemp=((int)(wy*100))/100.0;
        double ztemp=((int)(wz*100))/100.0;
        return ":Z:("+xtemp+","+ytemp+","+ztemp+")";
    }
    public void setName(String s)
    {
        name=s;
    }
    public String getName()
    {
        String s= name;
        
        return s;
    }
    public void setIsSelected(boolean b)
    {
        isSelected=b;
    }
    public boolean isSelected()
    {
        return isSelected;
    }
    Scanner scanner=new Scanner(System.in);
    public void rotate(double rotX, double rotY, double rotZ, double shiftX, double shiftY,double shiftZ)
    {
        translate(-shiftX,-shiftY,-shiftZ);
        
        
        //System.out.println(rotX+","+rotY+","+rotZ+": ");
        rotate( rotX,  rotY,  rotZ);
        translate(shiftX,shiftY,shiftZ);
        //System.out.println(wx+","+wy+","+wz);


        //return tempMatrix3;
        
    }
    public void rotate(double rotX, double rotY, double rotZ)
    {
        //System.out.println(rotX+","+rotY+","+rotZ+": ");
        Matrix inputMatrix=new Matrix(new double[][]{{tx,ty,tz,1}});
        inputMatrix=inputMatrix.transpose();
        Matrix rotMatrix = null;
        Matrix tempMatrix1 = null;
        Matrix tempMatrix2 = null;
        Matrix tempMatrix3 = null;
        double xSin = Math.sin(rotX);
        double ySin = Math.sin(rotY);
        double zSin = Math.sin(rotZ);
        double xCos = Math.cos(rotX);
        double yCos = Math.cos(rotY);
        double zCos = Math.cos(rotZ);

        rotMatrix = new Matrix(new double[][]{
            {yCos, 0, -ySin, 0},
            {0, 1, 0, 0},
            {ySin, 0, yCos, 0},
            {0, 0, 0, 1}
        });
        tempMatrix1 = rotMatrix.transpose().times(inputMatrix);
        
        //System.out.println();
        rotMatrix = new Matrix(new double[][]{
            {1, 0, 0, 0},
            {0, xCos, xSin, 0},
            {0, -xSin, xCos, 0},
            {0, 0, 0, 1}
        });
        tempMatrix2 = rotMatrix.transpose().times(tempMatrix1);
        rotMatrix = new Matrix(new double[][]{
            {zCos, zSin, 0, 0},
            {-zSin,zCos, 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1}
        });
        //HERE
        tempMatrix3=rotMatrix.transpose().times(tempMatrix2).transpose();
        //tempMatrix3.show();
        //tempMatrix3.show();
        //System.out.println();
        double[][] temp=tempMatrix3.getArray();
        //System.out.println();
        tx=temp[0][0];
        ty=temp[0][1];
        tz=temp[0][2];
        
        //System.out.println(wx+","+wy+","+wz);


        //return tempMatrix3;
    }
    public void translate(double x,double y, double z)
    {
        //System.out.println(x+","+y+","+z);
        Matrix inputMatrix=new Matrix(new double[][]{{tx,ty,tz,1}}).transpose();
        double[][] temp = new double[][]{
            {1, 0, 0, x},
            {0, 1, 0, y},
            {0, 0, 1, z},
            {0, 0, 0, 1}
        };
        Matrix m = new Matrix(temp);
        //System.out.println(m);
        Matrix tempMatrix=m.times(inputMatrix);
        double[][] temp2=tempMatrix.transpose().getArray();
        tx=temp2[0][0];
        ty=temp2[0][1];
        tz=temp2[0][2];
        
        //System.out.println(wx+","+wy+","+wz);
        
        //m=m.transpose();
        
    
    }
    public double getSX()
    {
        //System.out.println(wx+","+wy+","+wz);
        //System.out.println(isMidPoint);
        //if(!isMidPoint)
            sx=((wx*c)/wz);
        return sx;
        
    }
    public double getSY()
    {
        //System.out.println(wx+","+wy+","+wz);
        //System.out.println(isMidPoint);
        //if(!isMidPoint)
            sy=((wy*c)/wz);
        return sy;
        
    }
    public double getLX()
    {
        return lx;
    }
    public void setLX(double d)
    {
        lx=d;
    }
    public double getLY()
    {
        return ly;
    }
    public void setLY(double d)
    {
        ly=d;
    }
    public double getLZ()
    {
        return lz;
    }
    public void setLZ(double d)
    {
        lz=d;
    }
    public double getWX()
    {
        return wx;
    }
    public void setWX(double d)
    {
        wx=d;
    }
    public double getWY()
    {
        return wy;
    }
    public void setWY(double d)
    {
        wy=d;
    }
    public double getWZ()
    {
        return wz;
    }
    public void setWZ(double d)
    {
        wz=d;
    }
    public double getTX()
    {
        return tx;
    }
    public void setTX(double d)
    {
        tx=d;
    }
    public double getTY()
    {
        return ty;
    }
    public void setTY(double d)
    {
        ty=d;
    }
    public double getTZ()
    {
        return tz;
    }
    public void setTZ(double d)
    {
        tz=d;
    }
    public void setSX(double d)
    {
        sx=d;
    }
    public void setSY(double d)
    {
        sy=d;
    }
    public void convertFromScreenCoord(double x, double y, double z, double oriX, double oriY, double oriZ)
    {
        wz=z;
        wx=(x*z)/c;
        wy=(y*z)/c;
        tx=wx-oriX;
        ty=wy-oriY;
        tz=wz-oriZ;
        Vertex temp=new Vertex(tx,ty,tz);
        
        //System.out.println(wx+","+wy+","+wz);
    }
    private boolean isMidPoint=false;
    public void setIsMidpoint(boolean b)
    {
        isMidPoint=b;
    }
    public Vertex midPointVertex(Vertex other)
    {
        
        Vertex ret=new Vertex(0,0,0);
        ret.setName(this.getName()+other.getName());
        if(this.getName().indexOf(other.getName())!=-1)
        {
            return this;
        }
        else if(other.getName().indexOf(this.getName())!=-1)
        {
            return other;
        }
        ret.setIsMidpoint(true);
        ret.setSX((this.getSX()+other.getSX())/2.0);
        ret.setSY((this.getSY()+other.getSY())/2.0);
        ret.setLX((this.getLX()+other.getLX())/2);
        ret.setLY((this.getLY()+other.getLY())/2);
        ret.setLZ((this.getLZ()+other.getLZ())/2);
        
        ret.setTX((this.getTX()+other.getTX())/2);
        ret.setTY((this.getTY()+other.getTY())/2);
        ret.setTZ((this.getTZ()+other.getTZ())/2);
        
        ret.setWX((this.getWX()+other.getWX())/2);
        ret.setWY((this.getWY()+other.getWY())/2);
        ret.setWZ((this.getWZ()+other.getWZ())/2);
        return ret;
    }
    public int compareTo(Object t) {
        Vertex v=(Vertex)t;
        return (int)((v.getWZ()*10.0)-(10.0*getWZ()));
    }
    
}
