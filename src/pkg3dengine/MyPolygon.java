/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dengine;

import java.awt.*;
import java.util.*;

/**
 *
 * @author kingm14
 */
public class MyPolygon implements Comparable {
    public double oriX,oriY,oriZ;
    private Color color;
    private ArrayList<Vertex> vertices;
    public MyPolygon(Color c)
    {
        //System.out.println("1");
        color=c;
        vertices=new ArrayList<Vertex>();
    }
    public MyPolygon(Color c, ArrayList<Vertex> v)
    {
        
        //System.out.println("2");
        color=c;
        vertices=v;
    }
    public void setColor(Color c)
    {
        color=c;
    }
    public Color getColor()
    {
        return color;
    }
    public boolean doesContain(int x, int y)
    {
        Polygon p=new Polygon();
        
        Vertex closest=vertices.get(0);
        Vertex farthest=vertices.get(0);
        for(Vertex v:vertices)
        {
            if(v!=null)
            {
                p.addPoint((int)(v.getSX()), (int)(v.getSY()));
                
                
            }
        }
        return p.contains(x,y);
    }
    public String toString()
    {
        String s=vertices.get(0).getName();
        for(int i=1; i<vertices.size(); i++)
        {
            s+="->"+vertices.get(i).getName();
        }
        return s;
    }
    public void draw(Graphics g)
    {
        
        //g2d.setPaint(gradient);
        Polygon p=new Polygon();
        
        Vertex closest=vertices.get(0);
        Vertex farthest=vertices.get(0);
        for(Vertex v:vertices)
        {
            if(v!=null)
            {
                p.addPoint((int)(v.getSX()+350), (int)(v.getSY()+350));
                if(v.getLZ()<closest.getLZ())
                {
                    closest=v;
                }
                if(v.getLZ()==closest.getLZ())
                {
                    closest=closest.midPointVertex(v);
                }
                if(v.getLZ()>farthest.getLZ())
                {
                    farthest=v;
                }
                if(v.getLZ()==farthest.getLZ())
                {
                    farthest=farthest.midPointVertex(v);
                }
                
            }
        }
        Graphics2D g2d = (Graphics2D)g;
        Color s1 = color;
        
        
        /*for(int i=0; i<100; i++)
        {
            e=e.darker();
        }*/
        Paint temp=g2d.getPaint();
        /*GradientPaint gradient = null;
        //System.out.println(e.getRed()+""+(farthest.getLZ()*1000.0));
        for(int i=-2; i<closest.getTZ()*5; i++)
        {
            s1=s1.darker();
        }*/
        Color e = color;
        /*for(int i=-2; i<(farthest.getTZ())*5; i++)
        {
            e=e.darker();
        }*/
        //if(farthest.equals(closest))
        {
            g2d.setPaint(temp);
            g2d.setColor(e);
            g2d.fillPolygon(p);
        }
        /*else
        {
        gradient = new GradientPaint((int)(closest.getSX()+350), (int)(closest.getSY()+350),s1,(int)(farthest.getSX()+350), (int)(farthest.getSY()+350),e,false);
                    g2d.setPaint(gradient);
                    g2d.fillPolygon(p);
                    g2d.setColor(Color.red);
                    g2d.fillOval((int)(farthest.getSX()+350), (int)(farthest.getSY()+350), 10, 10);
                    g2d.setColor(Color.black);
                    g2d.drawString(farthest.getName(),(int)(farthest.getSX()+350), (int)(farthest.getSY()+350));
                    g2d.setColor(Color.green);
                    g2d.fillOval((int)(closest.getSX()+350), (int)(closest.getSY()+350), 10, 10);
                    g2d.setColor(Color.black);
                    g2d.drawString(closest.getName(),(int)(closest.getSX()+350), (int)(closest.getSY()+350));
                    g2d.drawLine((int)(closest.getSX()+350), (int)(closest.getSY()+350), (int)(farthest.getSX()+350), (int)(farthest.getSY()+350));
                    //System.out.println(closest.getName()+"==>"+farthest.getName());
        }*/
        
        /*for(Vertex s:vertices)
        {
            for(Vertex end:vertices)
            {
                if(s!=null&&end!=null&&s!=end)
                {
                    //g2d.drawLine((int)(s.getSX()+350), (int)(s.getSY()+350), (int)(end.getSX()+350), (int)(end.getSY()+350));
                    gradient = new GradientPaint((int)(end.getSX()+350), (int)(end.getSY()+350),s1,(int)(s.getSX()+350), (int)(s.getSY()+350),e,false);
                    g2d.setPaint(gradient);
                    g2d.fillPolygon(p);
                }
            }
            /*if(v!=null)
            {
                p.addPoint((int)(v.getSX()+350), (int)(v.getSY()+350));
                
            }*/
        //}
        //g2d.setColor(color);
        
        
        
        g2d.setPaint(temp);
        g2d.setColor(Color.black);
        g2d.drawPolygon(p);
        /*for(Vertex v:vertices)
        {
            if(v!=null)
            {
                g2d.setColor(Color.blue);
                    g2d.fillOval((int)(v.getSX()+350), (int)(v.getSY()+350), 10, 10);
                    g2d.setColor(Color.black);
                    g2d.drawString(v.getName(),(int)(v.getSX()+350), (int)(v.getSY()+350));
            }
        }*/
        
    }
    public boolean isBackfacing()
    {
        double cullMe,x1,x2,x3,y1,y2,y3,z1,z2,z3;
        Vertex v0,v1,v2;
        v0=vertices.get(0);
        v1=vertices.get(1);
        v2=vertices.get(2);
        x1=v0.getWX();
        x2=v1.getWX();
        x3=v2.getWX();
        
        y1=v0.getWY();
        y2=v1.getWY();
        y3=v2.getWY();
        
        z1=v0.getWZ();
        z2=v1.getWZ();
        z3=v2.getWZ();
        
        cullMe = x3  * ((z1*y2)-(y1*z2)) +
	     y3  * ((x1*z2)-(z1*x2)) +
	     z3  * ((y1*x2)-(x1*y2)) ;
        
        return (cullMe<0);
    }
    public ArrayList<Vertex> getVertices()
    {
        return vertices;
    }
    public void setVertices(ArrayList<Vertex> v)
    {
        vertices=v;
    }
    public void resetVertexRotBool()
    {
        for(Vertex v:vertices)
        {
            //System.out.println(v);
            if(v!=null)
            {
                
                v.setBeenRotated(false);
            }
        }
    }
    public void rotate(double rotX, double rotY,double rotZ)
    {
        for(Vertex v:vertices)
        {
            //System.out.println(v);
            if(v!=null&&!v.hasBeenRotated())
            {
                v.rotate(rotX, rotY, rotZ);
                v.setBeenRotated(true);
            }
        }
    }
    public void rotate(double rotX, double rotY, double rotZ, double oriX, double oriY,double oriZ)
    {
        for(Vertex v:vertices)
        {
            //System.out.println(v);
            if(v!=null&&!v.hasBeenRotated())
            {
                v.rotate(rotX, rotY, rotZ,oriX,oriY,oriZ);
                v.setBeenRotated(true);
            }
        }
    }
    public void translate(double x, double y,double z)
    {
        for(Vertex v:vertices)
        {
            if(v!=null)
                v.translate(x, y, z);
        }
    }
    public void setToDefault()
    {
        for(Vertex v:vertices)
        {
            if(v!=null)
            {
                v.setTX(v.getLX());
                v.setTY(v.getLY());
                v.setTZ(v.getLZ());
            }
        }
        
    }
    public double getAveDepth()
    {
        double x=0;
        for(Vertex v: vertices)
        {
            x+=v.getWZ();
        }
        return ((x+0.0)/vertices.size());
    }
    public double getMaxZ()
    {
        double x=0;
        for(Vertex v: vertices)
        {
            if(v.getWZ()>x)
            {
                x=(v.getWZ()*100.0);
            }
            //x+=v.getWZ();
        }
        return x;
    }
    public int compareTo(Object t) {
        /*if(!(t instanceof MyPolygon))
        {
            throw new Exception e;
        }*/
        ArrayList<Vertex> temp=(ArrayList<Vertex>)vertices.clone();
        
        //Collections.copy(temp,vertices);
        Collections.sort(temp);
        MyPolygon p=(MyPolygon)t;
        ArrayList<Vertex> list=(ArrayList<Vertex>)p.getVertices().clone();
        Collections.sort(list);
        Vertex myClosest=temp.get(0);
        int myR=1;
        //System.out.print(this.getColor().toString()+"=mine:");
        Vertex othersClosest=list.get(0);
        int otherR=1;
        for(Vertex i:temp)
        {
            //System.out.print(i.getWZ()+",");
            if(i.getWZ()>myClosest.getWZ())
            {
                myClosest=i;
                myR=1;
            }
            if(i.getWZ()==myClosest.getWZ())
            {
                myR++;
            }
        }
        /*System.out.println("| myClosest="+myClosest.getWZ());
        System.out.print(this.getColor().toString()+"other:");*/
        for(Vertex i:list)
        {
            //System.out.print(i.getWZ()+",");
            if(i.getWZ()>othersClosest.getWZ())
            {
                othersClosest=i;
                otherR=1;
            }
            if(i.getWZ()==myClosest.getWZ())
            {
                otherR++;
            }
            
        }
        //System.out.println("| othersClosest="+othersClosest.getWZ());
        
        /*if((int)((othersClosest.getWZ()-myClosest.getWZ())*100)==0)
        {
            System.out.println("in if"+(otherR-myR));
             System.out.println();
            return myR-otherR;
        }*/
        //System.out.println((int)((othersClosest.getWZ()-myClosest.getWZ())*100));
        //System.out.println();
        return (int)((othersClosest.getWZ()-myClosest.getWZ())*100);
        /*int size=temp.size();
        if(list.size()<size)
        {
            size=list.size();
        }
        for(int i=0; i<size; i++)
        {
            if(temp.get(i).getWZ()>list.get(i).getWZ())
            {
                return -1;
            }
            else if(temp.get(i).getWZ()<list.get(i).getWZ())
            {
                return 1;
            }
        }
        return 0;*/
        //return ( p.getAveDepth()-this.getAveDepth());
        //return 0;
        
    }
    public ArrayList<Vertex> getVertexAt(double x, double y, double error)
    {
        ArrayList<Vertex> retList=new ArrayList<Vertex>();
        retList.add(vertices.get(0));
        for(Vertex v:vertices)
        {
            Vertex ret=retList.get(0);
            //System.out.println(Math.abs(x-v.getSX())+" "+Math.abs(y-v.getSY()));
            //if(Math.abs(x-v.getSX())<error && Math.abs(y-v.getSY())<error)
            //{
                Point p0=new Point((int)x,(int)y);
                Point p1=new Point((int)v.getSX(),(int)v.getSY());
                Point p2=new Point((int)ret.getSX(),(int)ret.getSY());
                if(p0.distance(p1)<p0.distance(p2))
                {
                    retList.clear();
                    retList.add(v);
                }
                else if(ret.getWX()==v.getWX()&&ret.getWY()==v.getWY()&&ret.getWZ()==v.getWZ())
                {
                    retList.add(v);
                }
            //}
        }
        return retList;
    }
    
    public void drawNodes(Graphics g)
    {
        for(Vertex v:vertices)
        {
            /*Font f=g.getFont();
            g.setFont(new Font(f.getFontName(),f.getStyle(),10));
            String blank="";
            for(int i=0; i<v.getName().length()+1;i++)
            {
                blank+=" ";
            }
            int width=g.getFontMetrics().stringWidth(v.getName()+v.getLString());
            int temp=g.getFontMetrics().stringWidth(blank+v.getTString());
            if(temp>width)
            {
                width=temp;
            }
            temp=g.getFontMetrics().stringWidth(blank+v.getWString());
            if(temp>width)
            {
                width=temp;
            }
            g.setColor(Color.LIGHT_GRAY);
            {
                
                g.fill3DRect((int)(v.getSX()-6)+350-1, (int)(v.getSY()-6)+350-1-g.getFont().getSize(),width+2,(g.getFont().getSize())+4,true);
            }
            g.setColor(Color.red);
            
            
            //g.drawString(v.getName()+v.getLString(), (int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350);
            //g.drawString(v.getName()+v.getTString(), (int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350+g.getFont().getSize());
            g.drawString(v.getName()+v.getWString(), (int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350);
            */
           g.setColor(new Color(135,206,250));
            //g.drawOval((int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350, 12, 12);
            if(v.isSelected())
            {
                g.setColor(Color.RED);
            }
            g.fillOval((int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350, 12, 12);
            g.setColor(new Color(135,206,250));
            g.drawOval((int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350, 12, 12);
            
            
        }
    }
    public void drawPositions(Graphics g)
    {
        for(Vertex v:vertices)
        {
            Font f=g.getFont();
            g.setFont(new Font(f.getFontName(),f.getStyle(),10));
            String blank="";
            for(int i=0; i<v.getName().length()+1;i++)
            {
                blank+=" ";
            }
            int width=g.getFontMetrics().stringWidth(v.getName()+v.getLString());
            int temp=g.getFontMetrics().stringWidth(blank+v.getTString());
            if(temp>width)
            {
                width=temp;
            }
            temp=g.getFontMetrics().stringWidth(blank+v.getWString());
            if(temp>width)
            {
                width=temp;
            }
            g.setColor(Color.LIGHT_GRAY);
            {
                
                g.fill3DRect((int)(v.getSX()-6)+350-1, (int)(v.getSY()-6)+350-1-g.getFont().getSize(),width+2,(g.getFont().getSize())+4,true);
            }
            g.setColor(Color.red);
            
            
            //g.drawString(v.getName()+v.getLString(), (int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350);
            //g.drawString(v.getName()+v.getTString(), (int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350+g.getFont().getSize());
            g.drawString(v.getName()+v.getWString(), (int)(v.getSX()-6)+350, (int)(v.getSY()-6)+350);
            
            
            
        }
    }
    
}
