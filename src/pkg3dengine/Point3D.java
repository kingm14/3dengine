/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dengine;

/**
 *
 * @author kingm14
 */
public class Point3D {
    double x,y,z;
    Matrix m;
    public Point3D(double x, double y, double z)
    {
        this.x=x;
        this.y=y;
        this.z=z;
        double[][] mTemp=new double[4][1];
        mTemp[0][0]=x;
        mTemp[1][0]=y;
        mTemp[2][0]=z;
        mTemp[0][0]=1;
        m=new Matrix(mTemp);
    }
    public Point3D(Matrix m)
    {
        this.m=m;
        x=m.getArray()[0][0];
        y=m.getArray()[1][0];
        z=m.getArray()[2][0];
    }
    public void setX(double d){
        x=d;
        m.set(0, 0, d);
        
    }
    public void setY(double d){
        y=d;
        m.set(1, 0, d);
    }
    public void setZ(double d){
        z=d;
        m.set(2, 0, d);
    }
    public double getX()
    {
        return x;
    }
    public double getY()
    {
        return y;
    }
    public double getZ()
    {
        return z;
    }
    public Matrix getMatrix()
    {
        return m;
    }
    public double[] toArray()
    {
        return new double[]{x,y,z,1};
    }
    public String toString()
    {
        return ("("+x+","+y+","+z+")");
    }
    
}
